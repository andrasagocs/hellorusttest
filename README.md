This is an extremely simple example on how to use rust and snaps together.
You may try this at home, but don't use it in production (I don't see how someone would need a "hello world" application in production though).
The default cargo application is used with the simplest snapcraft.yaml file I could think of.

Usage
=====

Just to try and compile:

    $ cargo build
    $ cargo run

If you really like it:

    $ cargo install

If you no longer like it:

    $ cargo uninstall hellosnap

Create a snap:

    $ snapcraft

Install the snap locally:

    $ sudo snap install --dangerous hellorusttest_0.0.1_*.snap

Notes
=====

 * The name of the cargo project ("hellosnap") doesn't match the name of the snap ("hellorusttest") but this doesn't seem to be a problem in my setup.
 * The locally created snap is unsigned so the `--dangerous` option is required to install it.
 * There is really nothing special about these files, I just combined the first page of the rust tutorial with the snap tutorial.

References
==========

 * [The rust language](https://www.rust-lang.org/en-US/)
 * [The rust tutorial](https://doc.rust-lang.org/book/guessing-game.html)
 * [Snap](http://snapcraft.io/)
 * [Learn to make a snap](http://snapcraft.io/create/)
